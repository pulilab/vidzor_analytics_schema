Vidzor Analytics Schema
=========================

This python module provides the MongoDB schemas for Vidzor Analytics.

For support on its use, please contact Vidzor.

Created by
--------------

* `Vidzor Studio LLC <http://vidzor.com>`
