# This file should be kept separately as we might use it oustide of django too to access the MongoDB schemas
from mongoengine import *
from mongoengine import signals
import datetime


class DateTimeField(DateTimeField):
    def to_mongo(self, value):
        if isinstance(value, int):
            return datetime.datetime.fromtimestamp(int(value)/1000.0)
        return super(DateTimeField, self).to_mongo(value)


class Event(EmbeddedDocument):
    """
    Event embedded document to store various events within a view
    """
    event_type = StringField(max_length=32)
    start = DateTimeField()
    end = DateTimeField()
    last_position = FloatField()
    duration = FloatField()


class KeyValue(EmbeddedDocument):
    """
    Key-Value document to store additional data for apps
    """
    key = StringField(max_length=255)
    value = StringField(max_length=255)


class Data(EmbeddedDocument):
    """
    Embedded document allows another embedded key-value pairs to be added
    """
    app_type = StringField(max_length=32)
    app_instance = StringField(max_length=64)
    app_name = StringField(max_length=256)
    action_happened = BooleanField()
    current_time = DateTimeField()
    keyvalues = ListField(EmbeddedDocumentField(KeyValue))


class Viewer(EmbeddedDocument):
    """
    Embedded document allows another embedded key-value pairs to be added
    """
    viewer_id = StringField(max_length=64)
    action_happened = BooleanField()
    current_time = DateTimeField()
    keyvalues = ListField(EmbeddedDocumentField(KeyValue))


class AppStats(Document):
    """
    Model to store stats by app
    """
    vidzio_id = IntField(required=True)
    video_id = StringField(max_length=32, required=True)
    app_name = StringField(max_length=256)
    app_type = StringField(max_length=32)
    app_instance = StringField(max_length=64)
    viewers = ListField(EmbeddedDocumentField(Viewer))

    @classmethod
    def set_app_type(cls, vidzio_id, video_id, app_instance, app_type, app_name):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id, app_instance=app_instance)\
            .update_one(set__app_type=app_type, set__app_name=app_name, upsert=True)

    @classmethod
    def add_viewer_to_set(cls, vidzio_id, video_id, app_instance, viewer):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id, app_instance=app_instance)\
            .update_one(push__viewers=viewer)


class VideoStats(Document):
    """
    Model to store stats of each video
    """
    vidzio_id = IntField(required=True)
    video_id = StringField(max_length=32, required=True)
    video_length = FloatField(help_text="Length in seconds")
    file_size = DecimalField(help_text="Size in kB")
    num_views = IntField(default=0)
    num_unique_views = IntField(default=0)
    num_full_views = IntField(help_text="Only incremented if the video "
                                        "is played till the very end")
    num_impressions = IntField(default=0)
    browser = DictField()
    os = DictField()
    country = DictField()
    daily_views = DictField()

    attention_span = FloatField()
    last_calculated = DateTimeField()
    sum_last_position = FloatField(default=0)

    meta = {'object_name': 'VideoStat', 'app_name': 'analytics'}

    def __unicode__(self):
        return u'Video stats for %s (vidzio: %s)' % (self.video_id, self.vidzio_id)

    @classmethod
    def inc_num_views(cls, vidzio_id, video_id):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id)\
            .update_one(inc__num_views=1, upsert=True)

    @classmethod
    def inc_unique_views(cls, vidzio_id, video_id):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id)\
            .update_one(inc__num_unique_views=1, upsert=True)

    @classmethod
    def inc_num_full_views(cls, vidzio_id, video_id):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id)\
            .update_one(inc__num_full_views=1)

    @classmethod
    def inc_num_impressions(cls, vidzio_id, video_id):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id)\
            .update_one(inc__num_impressions=1)

    @classmethod
    def set_video_length(cls, vidzio_id, video_id, duration):
        cls.objects(vidzio_id=vidzio_id, video_id=video_id)\
            .update_one(set__video_length=duration)

    @classmethod
    def inc_stat(cls, vidzio_id, video_id, **kwargs):
        if 'day' in kwargs:
            key = 'inc__daily_views__%s' % kwargs['day']
        elif 'os' in kwargs:
            key = 'inc__os__%s' % kwargs['os']
        elif 'browser' in kwargs:
            key = 'inc__browser__%s' % kwargs['browser']
        elif 'country' in kwargs:
            key = 'inc__country__%s' % kwargs['country']
        else:
            raise NotImplementedError

        cls.objects(vidzio_id=vidzio_id, video_id=video_id)\
            .update_one(**{key: 1})

class ViewerStats(Document):
    """
    Model to store stats of a viewer
    """
    viewer_id = StringField(max_length=64, required=True)
    returning_id = StringField(max_length=64)
    vidzio_id = IntField(required=True)
    video_id = StringField(max_length=32, required=True)

    start = DateTimeField()
    end = DateTimeField()
    last_position = FloatField(default=0)
    duration = FloatField(required=False)

    browser = StringField(max_length=32)
    os = StringField(max_length=32)
    lat = FloatField()
    lon = FloatField()
    country = StringField(max_length=64)
    city = StringField(max_length=64)

    events = ListField(EmbeddedDocumentField(Event))
    data = ListField(EmbeddedDocumentField(Data))

    meta = {'object_name': 'ViewerStat', 'app_name': 'analytics'}

    def __unicode__(self):
        return u'Viewer stats for %s' % self.viewer_id

    @classmethod
    def post_save(cls, sender, document, created):
        """
        The main part where the other two models are created or updated with
        atomic mongoDB inserts.
        """
        kwargs = {"vidzio_id": document.vidzio_id,
                  "video_id": document.video_id}

        ### VideoStats PART:
        if created:
            # update browser, os, country, dailyviews
            VideoStats.inc_stat(os=document.os, **kwargs)
            VideoStats.inc_stat(browser=document.browser, **kwargs)
            VideoStats.inc_stat(country=document.country, **kwargs)

signals.post_save.connect(ViewerStats.post_save, sender=ViewerStats)


class BandwidthStats(Document):
    file_id = IntField(required=True)
    file_path = StringField()
    vidzio_id = IntField(required=False)
    type = StringField(required=True)
    owner_uuid = StringField(required=True)
    date = DateTimeField(required=True)
    bandwidth = IntField(required=True, default=0)


class BandwidthDaily(Document):
    date = DateTimeField(required=True)
    bandwidth = IntField(required=True, default=0)
    time_spent = FloatField()
    users = ListField(StringField())

