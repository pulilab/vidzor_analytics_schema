"""
This python module provides the Analytics schemas used by Vidzor.

It's provided here to help self-hosted Vidzor Analytics solutions.
"""
from setuptools import setup

setup(
    name="Vidzor-Analytics-Schema",
    version="2.4.1",
    description='MongoDB Schemas for Vidzor Analytics implementations',
    long_description=__doc__,
    author="Viktor Nagy",
    author_email='v@vidzor.com',
    url='https://bitbucket.com/pulilab/vidzor_analytics_schema',
    include_package_data=True,
    zip_safe=False,
    install_requires=['mongoengine'],
    py_modules=['vidzor_analytics_schema'],
    keywords="s3stat amazon statistics goaccess"
    # tests_require=['pytest'],
    # cmdclass = {
    #     'test': PyTest,
    # }
)